function cacheFunction(cb) {
  let store = {}
  function call(n) {
    if (store[n] === undefined) {
      store[n] = cb(n)
      return store[n]
    } else {
      return store[n]
    }
  }
  return call
}

module.exports = cacheFunction
