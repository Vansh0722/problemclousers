function counterFactory() {
  count = 10;

  function increment() {
    return ++count
  }
  function decrement() {
    return --count
  }
  return {
    "10 is incremented and the value is": increment(),
    "11 is decremented and the value is": decrement(),
  };
}

module.exports = counterFactory
