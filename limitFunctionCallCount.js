function limitFunctionCallCount(cb, n) {
  let Count = 0;
  function call() {
    Count++;
    if (Count <=n) {
      return cb(5, 4)
    } else {
      return null
    }
  }
  return call
}

module.exports = limitFunctionCallCount
