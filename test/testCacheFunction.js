const cacheFunction = require("../cacheFunction.js")

function square(val) {
  return val*val
}

const result = cacheFunction(square)

console.log(result(3))
console.log(result(5))
console.log(result(3))
